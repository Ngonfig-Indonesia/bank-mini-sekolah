@extends('/portal/app')
@section('content')
<div class="container">
  <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
      @foreach ($data as $item)
      <div class="carousel-item active">
        <img src="{{ asset('storage/slide/'.$item->gambar) }}" class="d-block w-100" alt="..." height="500px">
        <div class="carousel-caption d-none d-md-block">
          <h3>{{ $item->judul}}</h3>
        </div>
      </div>
      @endforeach
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>
  <div class="row">
    <h2 class="text-center mt-3">Galeri<strong style="color: green"> Sekolah</strong></h2>
    <div class="row">
      @foreach ($galeri as $galeris)
        <div class="col-4">
          <div class="text-center mt-2">
            <div class="">
              <img src="{{ asset('storage/images/'.$galeris->gambar) }}" alt="" width="300px" height="190px">
              <h4 class="card-title">{{ $galeris->judul }}</h4>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
  <div class="row">
    <h2 class="text-center mt-3">Berita <strong style="color: green">Terbaru</strong></h2>
    @foreach ($informasi as $item)
    <div class="card mb-3" style="max-width: 100%;">
      <div class="row g-0">
        <div class="col-md-4">
          <img src="{{ asset('storage/infromasi/'.$item->gambar) }}" class="img-fluid rounded-start" alt="...">
        </div>
        <div class="col-md-8">
          <div class="card-body">
            <h5 class="card-title"><a href="{{ route('informasi.detail',$item->id) }}" style="text-decoration: none;">{{ $item->judul }}</a></h5>
            <p class="card-text">{!! Str::limit($item->informasi, 400)!!}</p>
            {{-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> --}}
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection