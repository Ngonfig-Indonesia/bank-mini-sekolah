@extends('/portal/app')
@section('content')
<div class="container">
    <div class="text-center">
        <h2>{{ $informasi->judul}}</h2>
    </div>
    <div class="text-center">
        <img src="{{ asset('storage/infromasi/'.$informasi->gambar) }}" alt="" width="50%">
    </div>
    <div class="">
        <p>{!! $informasi->informasi !!}</p>
    </div>
</div>
@endsection