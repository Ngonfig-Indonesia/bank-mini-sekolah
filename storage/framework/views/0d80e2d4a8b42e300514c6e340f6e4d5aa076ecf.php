
<?php $__env->startSection('content'); ?>
    <div class="container">
        <h2 class="mt-3">Edit <strong>Data Guru</strong></h2>
        <form action="<?php echo e(route('update.guru')); ?>" method="post" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="input-group mt">
                <span class="input-group-text">Nip</span>
                <input type="hidden" name="id" value="<?php echo e($guru->id); ?>">
                <input type="text" name="nip" class="form-control" value="<?php echo e($guru->nip); ?>" required>
            </div>
            <div class="input-group mt-2">
                <span class="input-group-text">Nama</span>
                <input type="text" name="nama" class="form-control" value="<?php echo e($guru->nama); ?>" required>
            </div>
            <div class="input-group mt-2">
                <span class="input-group-text">Alamat</span>
                <textarea name="alamat" id="" class="form-control"><?php echo e($guru->alamat); ?></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update Guru</button>
            </div>
        </form>    
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/template/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\websekolah\resources\views/admin/datasekolah/editguru.blade.php ENDPATH**/ ?>