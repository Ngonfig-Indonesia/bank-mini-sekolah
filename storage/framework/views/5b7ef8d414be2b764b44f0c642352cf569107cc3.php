
<?php $__env->startSection('content'); ?>
    <div class="container">
        <h2 class="mt-3">Edit <strong>Data Siswa</strong></h2>
        <form action="<?php echo e(route('update.siswa')); ?>" method="post" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="input-group mt">
                <span class="input-group-text">Nis</span>
                <input type="hidden" name="id" value="<?php echo e($siswa->id); ?>">
                <input type="text" name="nis" class="form-control" value="<?php echo e($siswa->nis); ?>" required>
            </div>
            <div class="input-group mt-2">
                <span class="input-group-text">Nama</span>
                <input type="text" name="nama" class="form-control" value="<?php echo e($siswa->nama); ?>" required>
            </div>
            <div class="input-group mt-2">
                <span class="input-group-text">Alamat</span>
                <textarea name="alamat" id="" class="form-control"><?php echo e($siswa->alamat); ?></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update Siswa</button>
            </div>
        </form>    
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/template/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\websekolah\resources\views/admin/datasekolah/editsiswa.blade.php ENDPATH**/ ?>