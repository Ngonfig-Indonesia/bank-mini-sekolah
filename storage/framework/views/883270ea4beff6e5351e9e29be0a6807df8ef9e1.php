
<?php $__env->startSection('content'); ?>
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAdmin')): ?>
<div class="home mt-3">
  <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview"> 
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="statistics-details d-flex align-items-center justify-content-between">
                        <div>
                          <p class="statistics-title">Total Nasabah</p>
                          <h3 class="rate-percentage"><?php echo e($nasabah); ?></h3>
                        </div>
                        <div>
                          <p class="statistics-title">Total Tabungan Simpan</p>
                          <h3 class="rate-percentage"><?php
                              $hasil = 0;
                              foreach ($totalsimpan as $key) {
                                $hasil += $key->jumlah;
                              }
                               echo 'Rp '.number_format($hasil);
                          ?></h3>
                          
                        </div>
                        <div>
                          <p class="statistics-title">Total Transaksi Masuk</p>
                          <h3 class="rate-percentage"><?php echo e($totalmasuk); ?></h3>
                          
                        </div>
                      </div>
                    </div>
                  </div> 
                  
                </div>
      </div>
<?php elseif (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isTeller')): ?>
<h1>Selamat Datang Teller</h1>
<?php elseif (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAuthor')): ?>
<h1>Selamat Datang Admin</h1>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('../template/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\websekolah\resources\views//admin/home.blade.php ENDPATH**/ ?>