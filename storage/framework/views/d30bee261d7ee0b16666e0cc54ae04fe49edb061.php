
<?php $__env->startSection('content'); ?>
<div class="container">
    <h2>Data Guru</h2>
    <div class="row table-responsive">
        <table class="table table-striped" id="tabless">
            <thead>
                <tr>
                     <th class="scope">No</th>
                     <th class="scope">Nip</th>
                     <th class="scope">Nama</th>
                     <th class="scope">Alamat</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 0;
                ?>
                <?php $__currentLoopData = $guru; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                 <tr>
                    <td><?php echo e(++$no); ?></td>
                    <td><?php echo e($item->nip); ?></td>
                    <td><?php echo e($item->nama); ?></td>
                    <td><?php echo e($item->alamat); ?></td>
                 </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
     </div>
     <h2 class="mt-2">Data Siswa</h2>
    <div class="row table-responsive">
        <table class="table table-striped" id="tables">
            <thead>
                <tr>
                     <th class="scope">No</th>
                     <th class="scope">Nis</th>
                     <th class="scope">Nama</th>
                     <th class="scope">Alamat</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 0;
                ?>
                <?php $__currentLoopData = $siswa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                 <tr>
                    <td><?php echo e(++$no); ?></td>
                    <td><?php echo e($item->nis); ?></td>
                    <td><?php echo e($item->nama); ?></td>
                    <td><?php echo e($item->alamat); ?></td>
                 </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
     </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/portal/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\websekolah\resources\views/datasekolah.blade.php ENDPATH**/ ?>