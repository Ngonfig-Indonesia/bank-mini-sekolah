<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAdmin')): ?>
      <title>Bank Mini Syariah | SMK Negeri 1 Kota Bima</title>
    <?php elseif (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isTeller')): ?>
      <title>Bank Mini Syariah | SMK Negeri 1 Kota Bima</title>
    <?php else: ?>
      <title>Web Portal | SMK Negeri 1 Kota Bima</title>
    <?php endif; ?>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/dataTables.bootstrap5.min.css">
    
    </head>
  <body>
      <?php echo $__env->make('/template/navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <div class="container mt-3">
        <div class="row">
            <?php echo $__env->yieldContent('content'); ?>
        </div>
      </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/dataTables.bootstrap5.min.js"></script>
    
    <script>
      $(document).ready(function () {
          $('#table').DataTable();
          $('#tables').DataTable();
      });
    </script>
    <?php echo $__env->yieldPushContent('script'); ?>
  </body>
</html><?php /**PATH C:\xampp\htdocs\websekolah\resources\views//template/app.blade.php ENDPATH**/ ?>