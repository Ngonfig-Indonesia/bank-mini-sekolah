
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="text-center">
            <h2>Informasi <strong>Sekolah</strong></h2>
        </div>
        <?php $__currentLoopData = $informasis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="card mb-3" style="max-width: 100%;">
                <div class="row g-0">
                    <div class="col-md-4">
                    <img src="<?php echo e(asset('storage/infromasi/'.$item->gambar)); ?>" class="img-fluid rounded-start" >
                    </div>
                    <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo e($item->judul); ?></h5>
                        <p class="card-text"><?php echo Str::limit($item->informasi, 400); ?></p>
                        <a href="<?php echo e(route('informasi.detail',$item->id)); ?>">Read More</a>
                    </div>
                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <?php echo e($informasis->links()); ?>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/portal/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\websekolah\resources\views/informasi.blade.php ENDPATH**/ ?>