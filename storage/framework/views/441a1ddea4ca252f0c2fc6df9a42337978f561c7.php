
<?php $__env->startSection('content'); ?>
    <div class="container">
        <h2 class="mt-3">Edit <strong>Data Alumni</strong></h2>
        <form action="<?php echo e(route('alumni.update')); ?>" method="post" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="input-group mt">
                <span class="input-group-text">Nama</span>
                <input type="hidden" name="id" value="<?php echo e($data->id); ?>">
                <input type="text" name="nama" class="form-control" value="<?php echo e($data->nama); ?>" required>
            </div>
            <div class="input-group mt-2">
                <span class="input-group-text">Angkatan</span>
                <input type="text" name="angkatan" class="form-control" value="<?php echo e($data->angkatan); ?>" required>
            </div>
            <div class="input-group mt-2">
                <span class="input-group-text">Email</span>
                <input type="text" name="email" class="form-control" value="<?php echo e($data->email); ?>" required>
            </div>
            <div class="input-group mt-2">
                <span class="input-group-text">Alamat</span>
                <textarea name="alamat" id="" class="form-control"><?php echo e($data->alamat); ?></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update Alumni</button>
            </div>
        </form>    
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/template/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\websekolah\resources\views/admin/alumni/edit.blade.php ENDPATH**/ ?>