
<?php $__env->startSection('content'); ?>
    <div class="container">
        <h2 class="mt-3">Edit <strong>Halaman Informasi</strong></h2>
        <form action="<?php echo e(route('informasi.update')); ?>" method="post" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="input-group mt">
                <span class="input-group-text">Judul</span>
                <input type="hidden" name="id" value="<?php echo e($data->id); ?>">
                <input type="text" name="judul" class="form-control" value="<?php echo e($data->judul); ?>" required>
            </div>
            <div class="mt-2">
                <textarea name="informasi" class="form-control" id="my-editor" width="100%"><?php echo e($data->informasi); ?></textarea>
            </div>
            <button type="submit" class="btn btn-primary mt-2" value="Update">Update</button>
        </form>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
    <script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#my-editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('/template/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\websekolah\resources\views/admin/informasi/edit.blade.php ENDPATH**/ ?>