
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="text-center">
        <h2><?php echo e($informasi->judul); ?></h2>
    </div>
    <div class="text-center">
        <img src="<?php echo e(asset('storage/infromasi/'.$informasi->gambar)); ?>" alt="" width="50%">
    </div>
    <div class="">
        <p><?php echo $informasi->informasi; ?></p>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/portal/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\websekolah\resources\views/readmore.blade.php ENDPATH**/ ?>