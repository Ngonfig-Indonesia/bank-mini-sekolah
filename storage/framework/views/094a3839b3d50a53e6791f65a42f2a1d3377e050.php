<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid">
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAdmin')): ?>
    <a class="navbar-brand" href="<?php echo e(route('home')); ?>">
      <img src="https://www.smkn1kotabima.sch.id/images/gambar/logo_smkn_1_shadow.png" alt="" width="30" height="30" class="d-inline-block align-text-top">
      Bank Mini <strong>Syariah</strong>
    </a>
    <?php elseif (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isTeller')): ?>
    <a class="navbar-brand" href="<?php echo e(route('home')); ?>">
      <img src="https://www.smkn1kotabima.sch.id/images/gambar/logo_smkn_1_shadow.png" alt="" width="30" height="30" class="d-inline-block align-text-top">
      Bank Mini <strong>Syariah</strong>
    </a>
    <?php elseif (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAuthor')): ?>
    <a class="navbar-brand" href="<?php echo e(route('home')); ?>">
      Portal Web <strong>Jurusan</strong>
    </a>
    <?php endif; ?>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAdmin')): ?>
          <li class="nav-item dropdown">
            <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Nasabah
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="<?php echo e(route('tabungan.index')); ?>">Tabungan Masuk</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('transfer.index')); ?>">Transfer Saldo</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('tarik.index')); ?>">Tarik Saldo</a></li>
            </ul>
          </li>
          <?php elseif (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isTeller')): ?>
          <li class="nav-item dropdown">
            <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Nasabah
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="<?php echo e(route('tabungan.index')); ?>">Tabungan Masuk</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('transfer.index')); ?>">Transfer Saldo</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('tarik.index')); ?>">Tarik Saldo</a></li>
            </ul>
          </li>
          <?php else: ?>

          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAdmin')): ?>
          <li class="nav-item dropdown">
            <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Pengecekan Saldo
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="<?php echo e(route('saldo.index')); ?>">Saldo Nasabah</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('totaltabungan.index')); ?>">Total Tabungan Simpan</a></li>
            </ul>
          </li>
          <?php elseif (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isTeller')): ?>
          <li class="nav-item dropdown">
            <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Pengecekan Saldo
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="<?php echo e(route('saldo.index')); ?>">Saldo Nasabah</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('totaltabungan.index')); ?>">Total Tabungan Simpan</a></li>
            </ul>
          </li>
          <?php else: ?>
          <?php endif; ?>
          
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAdmin')): ?>
          <li class="nav-item dropdown">
            <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Data Nasabah
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="<?php echo e(route('nasabah.index')); ?>">Nasabah</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('jurusan.index')); ?>">Jurusan</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('nasabahaktif.index')); ?>">Nasabah Aktif/Non Aktif</a></li>
            </ul>
          </li>
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAdmin')): ?>
          <li class="nav-item dropdown">
            <a class="nav-link active" href="<?php echo e(route('user.index')); ?>">
              Data User
            </a>
          </li>
          <?php endif; ?>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAuthor')): ?>
          <li class="nav-item dropdown">
            <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Data Jurusan
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="<?php echo e(route('profil.show')); ?>">Profil</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('galeri.show')); ?>">Galeri</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('slide.show')); ?>">Slide Show</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('informasi.index')); ?>">Informasi</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('siswa.guru.index')); ?>">Siswa/Guru</a></li>
              <li><a class="dropdown-item" href="<?php echo e(route('alumni')); ?>">Alumni</a></li>
            </ul>
          </li>
          <?php endif; ?>
        </ul>
          
        <div class="d-flex">
          <div class="btn-group">
            <button type="button" class="btn btn-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="bi bi-person-circle"></i>
              Hi <strong><?php echo e(Auth::user()->name); ?></strong>
            </button>
            <ul class="dropdown-menu dropdown-menu-end">
              
              <li><a href="<?php echo e(route('logout')); ?>" class="dropdown-item" onclick="return confirm('Yakin Anda ingin Keluar?')">Logout</a></li>
              <!-- <li><button class="dropdown-item" type="button">Something else here</button></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
</nav><?php /**PATH C:\xampp\htdocs\websekolah\resources\views//template/navbar.blade.php ENDPATH**/ ?>