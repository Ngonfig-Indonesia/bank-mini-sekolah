
<?php $__env->startSection('content'); ?>

<h1>Nasabah<strong> Aktif / Non Aktif</strong></h1>
<div class="table-responsive">
    <table class="table table-striped" id="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Nis</th>
            <th scope="col">Kelas</th>
            <th scope="col">Jurusan</th>
            <th scope="col">Status</th>
            <th scope="col">Aktif/Non Aktif</th>
          </tr>
        </thead>
        <tbody>
        <?php
            $no = 0;
        ?>
          <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <th scope="row"><?php echo e(++$no); ?></th>
                <td><?php echo e($item->nama); ?></td>
                <td><?php echo e($item->nis); ?></td>
                <td><?php echo e($item->kelas->kelas); ?></td>
                <td><?php echo e($item->jurusan->jurusan); ?></td>
                <td><?php echo e($item->status); ?></td>
                <td>
                    <?php
                        if ($item->id == $item->tabunganmasuk[0]->id_nasabah || $item->transfer_pengirim[0]->id_pengirim) {
                            echo "Aktif";
                        }else {
                            echo "Tidak Aktif";
                        }
                    ?>
                </td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('../template/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\websekolah\resources\views//admin/nasabahaktif/index.blade.php ENDPATH**/ ?>